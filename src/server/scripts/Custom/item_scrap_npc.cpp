#include <DatabaseEnv.h>
#include <ScriptMgr.h>
#include <ScriptedCreature.h>
#include <Player.h>
#include <Item.h>
#include <Chat.h>
#include <ScriptedGossip.h>
#include <Log.h>
#include <SharedDefines.h>
#include <Random.h>

#include <vector>
#include <unordered_set>
#include <iomanip>
#include <sstream>

static const std::vector<uint8> WEAPONRY_SLOTS = std::vector<uint8>{
	EQUIPMENT_SLOT_MAINHAND,
	EQUIPMENT_SLOT_OFFHAND,
	EQUIPMENT_SLOT_RANGED
};

static const std::vector<uint8> ARMORY_SLOTS = std::vector<uint8>{
	EQUIPMENT_SLOT_HEAD,
	EQUIPMENT_SLOT_SHOULDERS,
	EQUIPMENT_SLOT_BACK,
	EQUIPMENT_SLOT_CHEST,
	EQUIPMENT_SLOT_BODY,
	EQUIPMENT_SLOT_TABARD,
	EQUIPMENT_SLOT_WRISTS,
	EQUIPMENT_SLOT_HANDS,
	EQUIPMENT_SLOT_WAIST,
	EQUIPMENT_SLOT_LEGS,
	EQUIPMENT_SLOT_FEET
};

static const std::vector<uint8> JEWELRY_SLOTS = std::vector<uint8>{
	EQUIPMENT_SLOT_NECK,
	EQUIPMENT_SLOT_FINGER1,
	EQUIPMENT_SLOT_FINGER2,
	EQUIPMENT_SLOT_TRINKET1,
	EQUIPMENT_SLOT_TRINKET2,
};

enum ACTION {
	MAIN_MENU 		= 1000,
	RELOAD 			= 1001,
	NEVERMIND 		= 1002,
	BACK 			= 1003,
	SCRAP			= 1004,
	MAIN_MENU_MENU 	= 1100,
	SLOTS 			= 1200,
	RECIPES 		= 2000,
};

struct scrap {
	scrap() { }
	uint32 currency;
	float success;
};

static std::unordered_map<uint32, std::vector<scrap>> depository = { };

static const void load_scrap_from_db() {
	QueryResult scrapq = WorldDatabase.PQuery("SELECT item_entry, currency, success"
		" FROM item_scrap_system;");
	
	if(scrapq) {
		depository.clear();
		do {
			Field* scrapf = scrapq->Fetch();

			uint32 item_entry = scrapf[0].GetUInt32();
            scrap s;
			s.currency = scrapf[1].GetUInt32();
            s.success = scrapf[3].GetFloat();
			depository[item_entry].push_back(s);
		} while(scrapq->NextRow());
	}
}

//returns a string with the hexadecimal representation of the inputed number in the decimal representation
static const std::string d2x(uint32 decimal) {
	std::stringstream ss;
	ss << std::hex << decimal;
	return ss.str();
}

static const std::string get_icon_name(uint8 slot, bool relic, bool shield) {
	std::string icon;

	switch(slot) {
		case EQUIPMENT_SLOT_HEAD: 		icon = "Head"; break;
		case EQUIPMENT_SLOT_NECK: 		icon = "Neck"; break;
		case EQUIPMENT_SLOT_SHOULDERS: 	icon = "Shoulder"; break;
		case EQUIPMENT_SLOT_BODY: 		icon = "Shirt"; break;
		case EQUIPMENT_SLOT_CHEST: 
		case EQUIPMENT_SLOT_BACK: 		icon = "Chest"; break;
		case EQUIPMENT_SLOT_WAIST: 		icon = "Waist"; break;
		case EQUIPMENT_SLOT_LEGS: 		icon = "Legs"; break;
		case EQUIPMENT_SLOT_FEET: 		icon = "Feet"; break;
		case EQUIPMENT_SLOT_WRISTS: 	icon = "Wrists"; break;
		case EQUIPMENT_SLOT_HANDS: 		icon = "Hands"; break;
		case EQUIPMENT_SLOT_FINGER1:
		case EQUIPMENT_SLOT_FINGER2: 	icon = "Finger"; break;
		case EQUIPMENT_SLOT_TRINKET1:
		case EQUIPMENT_SLOT_TRINKET2: 	icon = "Trinket"; break;
		case EQUIPMENT_SLOT_MAINHAND: 	icon = "MainHand"; break;
		case EQUIPMENT_SLOT_OFFHAND: 	icon = shield ? "SecondaryHand" : "MainHand"; break;
		case EQUIPMENT_SLOT_RANGED: 	icon = relic ? "Relic" : "Ranged"; break;
		case EQUIPMENT_SLOT_TABARD: 	icon = "Tabard"; break;
		default: icon = "EmptySlot";
	}

	return "|TInterface\\PaperDoll\\UI-PaperDoll-Slot-" + icon + ":32:32:-25:1|t";
};

static const std::string get_item_display(uint32 entry) {
	QueryResult itemq = WorldDatabase.PQuery("SELECT name, quality FROM item_template WHERE entry = " + std::to_string(entry) + ";");

	std::string item_display = "";
	if(itemq) {
		Field* fields = itemq->Fetch();

		item_display = "|c" + d2x(ItemQualityColors[fields[1].GetUInt8()]) + fields[0].GetString() + "|r";
	}

	return item_display;
}

struct player_meta {
	ObjectGuid selected_item;
	std::stack<uint32> breadcrumbs;
};

static std::unordered_map<ObjectGuid, player_meta> data = { };

class item_scrap_npc : public CreatureScript {
    public:
        item_scrap_npc() : CreatureScript("item_scrap_npc") {}

        struct item_scrap_npc_ai : public ScriptedAI {
            item_scrap_npc_ai(Creature* creature) : ScriptedAI(creature) { }

        };
        CreatureAI* GetAI(Creature* creature) const override {
		    return new item_scrap_npc_ai(creature);
	    }
};

void AddSC_item_scrap_npc() {
    new item_scrap_npc();
}
