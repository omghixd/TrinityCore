#include "ScriptMgr.h"
#include "AccountMgr.h"
#include "CharacterCache.h"
#include "Chat.h"
#include "DatabaseEnv.h"
#include "ObjectAccessor.h"
#include "ObjectMgr.h"
#include "Player.h"
#include "PlayerDump.h"
#include "World.h"
#include "Item.h"
#include "WorldSession.h"
#include "DBCStores.h"
#include "Log.h"
#include "RBAC.h"

#include <map>
#include <vector>

class infusion_commmandscript : public CommandScript {
	public:
		static std::map<uint32, uint32> upgrades;
        infusion_commmandscript() : CommandScript("infusion_commmandscript") {
			//TODO: load upgrades from the db!
            upgrades = {{22416, 599999}}; //t3 warr chest
        }
        std::vector<ChatCommand> GetCommands() const override {
          	static std::vector<ChatCommand> infusionCommandTable = {
              	{"check", rbac::RBAC_COMMAND_INFUSION_CHECK, false, &handle_check_command, ""},
              	{"fuse", rbac::RBAC_COMMAND_INFUSION_FUSE, false, &handle_fuse_command, ""}
			};
            static std::vector<ChatCommand> commandTable = {{"infusion", rbac::RBAC_COMMAND_INFUSION, false, NULL, "", infusionCommandTable},};
            return commandTable;
        }
		static unsigned int strtouint32(char *st) {
  			char *x;
  			for (x = st ; *x ; x++) {
    			if (!isdigit(*x))
     	 			return 0L;
  			}
  			return (strtoul(st, 0L, 10));
		}
        static bool handle_fuse_command(ChatHandler *handler, char const* args) {
			Player* player = handler->GetSession()->GetPlayer();
			char* item_id = strtok((char*)args, " ");
			char* item_bag = strtok(NULL, " ");
			char* item_slot = strtok(NULL, " ");
			char* infusion_has = strtok (NULL, " ");
			char* infusion_bag = strtok(NULL, " ");
			char* infusion_slot = strtok(NULL, " ");
			if( !item_id || !item_bag || !item_slot ) // necessary parameters
				return false;
			//TODO: check if the item entry is valid!
			uint32 u_item_id = strtouint32(item_id);
			uint8 u_item_bag = (uint8)atoi(item_bag);
			uint8 u_item_slot = (uint8)atoi(item_slot);
			Item* item = player->GetItemByPos(u_item_bag, u_item_slot);
			if(!item || item->GetEntry() != u_item_id) // checking if we got the valid item info
				return false;
			bool has_infusion = false;
			uint32 cnt = 1;
            if (infusion_has && atoi(infusion_has) == 1) {
                if (infusion_bag && infusion_slot) {
                  	Item* infusion = player->GetItemByPos((uint8)atoi(infusion_bag), (uint8)atoi(infusion_slot));
                    if (infusion && infusion->GetEntry() == 600002) {
                    	has_infusion = true;
						player->DestroyItemCount(infusion, cnt, true);
					}
                }
            }
			int chance = 5;
			if(has_infusion)
				chance = 2;
			srand(time(NULL));
			int success = rand()%chance;
            if (success == 0) { // upgrade item
				cnt = 1;
            	player->DestroyItemCount(item, cnt, true);
				player->AddItem(upgrades[u_item_id], 1);
				return true;
			}
			handler->SendSysMessage("@fail");
			return true;
        }
        static bool handle_check_command(ChatHandler *handler, char const* args) {
			Player* player = handler->GetSession()->GetPlayer();
			char* item_id = strtok((char*)args, " ");
			char* item_bag = strtok(NULL, " ");
			char* item_slot = strtok(NULL, " ");
			char* infusion_has = strtok(NULL, " ");
			char* infusion_bag = strtok(NULL, " ");
			char* infusion_slot = strtok(NULL, " ");
			if(!item_id || !item_bag || !item_slot)
				return false;
			uint32 u_item_id = strtouint32(item_id);
			uint8 u_item_bag = (uint8)atoi(item_bag);
			uint8 u_item_slot = (uint8)atoi(item_slot);
			Item* item = player->GetItemByPos(u_item_bag, u_item_slot);
			if(!item || item->GetEntry() != u_item_id)
				return false;
			bool has_infusion = false;
            if (infusion_has && infusion_bag && infusion_slot && atoi(infusion_has) == 1) {
            	Item* infusion = player->GetItemByPos((uint8)atoi(infusion_bag), (uint8)atoi(infusion_slot));
				if(infusion && infusion->GetEntry() == 600002)
					has_infusion = true;
			}
			handler->SendSysMessage(has_infusion ? "@1" : "@0");
			return true;
        }
};

void AddSC_cs_infusion() {
	new infusion_commmandscript();
}
